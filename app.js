let population, lifep, count, target, generations, genp

function setup() {
	createCanvas(CWDTH, CHGHT)
	population = new Population()
	lifep = createP()
	genp = createP()
	count = 0
	generations = 0
	target = createVector(CWDTH * .5, 50)
}

function draw() {
	background(BG)
	ellipse(target.x, target.y, 50)
	population.run()
	lifep.html(count)
	genp.html(generations)
	count += 1
	if (count === LIFESPAN) {
		population.evaluate()
		population.selection()
		count = 0
		generations += 1
	}
}