class Rocket {
	constructor(dna) {
		this.pos = createVector(CWDTH * .5, CHGHT)
		this.vel = createVector()
		this.acc = createVector()
		this.dna = dna ? dna : new DNA()
	}

	applyForce(force) {
		this.acc.add(force)
	}

	calcFitness() {
		let d = dist(this.pos.x, this.pos.y, target.x, target.y)
		this.fitness = map(d, 0, CWDTH, CWDTH, 0)
	}

	update() {
		this.applyForce(this.dna.genes[count])
		this.vel.add(this.acc)
		this.pos.add(this.vel)
		this.acc.mult(0)
	}

	show() {
		push()
		noStroke()
		fill(255, 125)
		translate(this.pos.x, this.pos.y)
		rotate(this.vel.heading())
		rectMode(CENTER)
		rect(0, 0, 50, 4)
		pop()
	}

	process() {
		this.update()
		this.show()
	}
}