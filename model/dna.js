class DNA {
    constructor(genes) {
        if (genes) this.genes = genes
        else this.genes = new Array(LIFESPAN).fill().map(g => {
            g = p5.Vector.random2D()
            g.setMag(.3)
            return g
        })
    }

    crossover(p) {
        const mid = floor(random(this.genes.length))
        const ngenes = this.genes.slice(0, mid).concat(p.genes.slice(mid))
        return new DNA(ngenes)
    }
}