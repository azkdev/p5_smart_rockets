class Population {
    constructor() {
        this.rockets = new Array(20).fill().map(r => new Rocket())
    }

    evaluate() {
        let maxFit = 0
        this.mates = new Array();
        
        this.rockets.map(r => {
            r.calcFitness()
            if (r.fitness > maxFit) maxFit = r.fitness
        })
        
        this.rockets = this.rockets.map(r => {
            r.fitness /= maxFit
            const n = r.fitness * 100
            for (let i = 0; i < n; i++) {
                this.mates.push(r)
            }
            return r
        })
    }

    selection() {
        this.rockets = this.rockets.map(r => {
            const p1 = random(this.mates).dna
            const p2 = random(this.mates).dna
            const child = p1.crossover(p2)
            console.log(child.genes.length)
            r = new Rocket(child)
            return r
        })
    }

    run() {
        this.rockets.map(r => r.process())
    }
}